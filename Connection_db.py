import psycopg2 

class Connection_db:
    def __init__(self,DB_host,name_database,DB_user,DB_password):
        
        try:
            self.conn= psycopg2.connect(
            host = DB_host,             
            database = name_database,
            user = DB_user,
            password = DB_password)
            self.objeto_cursor = self.conn.cursor() # Se creo el objeto cursor para ejecutar Querys a la DB 
            
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

    def search_db(self,query):
        try:
            self.objeto_cursor.execute(query)
            response= self.objeto_cursor.fetchall() #Para leer lo que retorna el query
            return response
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)


    def write_db(self,query):
        try:
            self.objeto_cursor.execute(query)
            self.conn.commit() # <- We MUST commit to reflect the inserted data
            #self.objeto_cursor.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
    
    def close_db(self,query):
        self.objeto_cursor.close()
        self.conn.close()