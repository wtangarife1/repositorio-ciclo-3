from Connection_db import Connection_db
from Crud import Crud
import time

Crud = Crud("localhost","Transportes","postgres","NECnec2021*$")
number_passenger = int(input("Ingrese la cantidad de pasajeros que desea incluir: "))
include=False
while include==False:
    for i in range (0,number_passenger):
        name= input("Ingrese el nombre del pasajero, debe ir entre comilla sencillas: ")
        adress= input("Ingrese la dirección del pasajero, debe ir entre comilla sencillas: ")
        phone = input("Ingrese número telefónico del pasajero, debe ir entre comilla sencillas: ")
        birthday=input("Ingrese la fecha de nacimiento del pasajero, debe ir entre comilla sencillas: ")
        number_passenger += 1
        #Crud.insert_traveler("'Mateo'","'Cra 87A 88-58'","'(310)-8175899'","'2020-02-26'")
        Crud.insert_traveler(name,adress,phone,birthday)
            
    include =True

# Crud.update_traveler("6","Julian Luna ","Cra 680","(321)-3555553","2020-3-11")
# Connection_db.close_db()

#LEER PÁSAJEROS DE MANERA ORGANIZADA
passengers=Crud.read_passenger()
for passenger in passengers:
    print(passenger)
Id=int(input("¿Que pasajero desea eliminar?: "))
Crud.delete_passenger(Id)
time.sleep(1)
for passenger in passengers:
    print(passenger)

#INSERTAR BUSES 
number_bus = int(input("Ingrese la cantidad de buses que desea incluir: "))
bus=False
while bus==False:
    for i in range (0,number_bus):
        model= int(input("Ingrese el modelo del vehículo:  "))
        capacity= int(input("Ingrese la capacidad del vehículo: "))
        vehicle_plate = input("Ingrese la placa del vehículo: ")
        car_brand=input("Ingrese la marca del vehículo: ")
        number_bus += 1
        Crud.insert_bus(model,capacity,vehicle_plate,car_brand)
            
    bus =True
#ACTUALIZAR BUSES 
Crud.update_bus(106,2012,6,"SSH-020","Renault")

#LEER BUSES 
buses=Crud.read_bus()
for bus in buses:
    print(bus)

#ELIMINAR BUSES POR ID 
Id_bus_delate=int(input("¿Que bus desea eliminar?: "))
Crud.delete_bus(Id_bus_delate)

time.sleep(1)
for bus in buses:
    print(bus)

#INSERTAR ESTACION
number_station = int(input("Ingrese la cantidad de estaciones que desea incluir: "))
station=False
while station==False:
    for i in range (0,number_station):
        name_station= input("Ingrese el nombre de la estación:  ")
        address_station= input("Ingrese la dirección de la estación: ")
        number_station += 1
        Crud.insert_station(name_station, address_station)
            
    station =True

#ACTUALIZAR ESTACION 
Crud.update_station(100,"Av Chile","Calle 75")

#LEER ESTACIONES 
stations=Crud.read_station()
for station in stations:
    print(station)

#ELIMINAR ESTACIÓN POR ID 
Id_st_delate=int(input("¿Que estación desea eliminar?: 0"))
Crud.delete_station(Id_st_delate)

time.sleep(1)
for station in stations:
    print(station)