
import psycopg2
from Connection_db import Connection_db
class Crud:
    #la clase encargada de contener las queries 
    # y ejecutarlas a traves de la clase conexion_db
    def __init__(self,DB_host,name_database,DB_user,DB_password ):
        self.conn = Connection_db(DB_host,name_database,DB_user,DB_password)#creo el objeto conn como atributo
        #el objeto conn es un objeto de la clase Conexion_db y un atributo de la clase Crud
        #se definen los metodos para escribir, leer, actualizar tablas 
#---------------------------------------------------------------------------------
    #PARA TABLA PASAJEROS
    def insert_traveler(self,Nombre,Direccion,Telefono,Fecha_nacimiento):
        query="INSERT INTO \"Pasajeros\""+\
        '("Nombre", "Direccion", "Telefono", "Fecha_nacimiento")'+\
        'VALUES ('"'"+Nombre+"'"','"'"+Direccion+"'"','"'"+Telefono+"'"','"'"+Fecha_nacimiento+"'"')'
        print (query)
        self.conn.write_db(query)
    
    def update_traveler(self,Id,Nombre,Direccion,Telefono,Fecha_nacimiento):
        print ("Hola")
        try:
            query="UPDATE \"Pasajeros\""+\
            'SET "Nombre" ='"'"+Nombre+"'"+\
                ',"Direccion"='"'"+Direccion+"'"+\
                ',"Telefono"='"'"+Telefono+"'"+\
                ',"Fecha_nacimiento"='"'"+Fecha_nacimiento+"'"+\
            'WHERE "Id" = '+Id
            self.conn.write_db(query)
            print(query) 
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

    def read_passenger(self):
        query ='SELECT * from \"Pasajeros\" order by "Id";'
        passengers=self.conn.search_db(query)
        return passengers

    def delete_passenger(self,Id):
        try:
            query="DELETE FROM \"Pasajeros\""+\
                'WHERE "Id" = '+str(Id)
            self.conn.write_db(query)
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
#_________________________________________________________________
#PARA TABLA BUSES
    def insert_bus(self,Modelo,Capacidad,Placa,Marca):
        try:
            query="INSERT INTO \"Buses\""+\
            '("Modelo", "Capacidad", "Placa", "Marca")'+\
            'VALUES ('"'"+str(Modelo)+"'"','"'"+str(Capacidad)+"'"','"'"+Placa+"'"','"'"+Marca+"'"')'
            print (query)
            self.conn.write_db(query)
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

    def update_bus(self,Id,Modelo,Capacidad,Placa,Marca):
            print ("Actualizando Bus")
            try:
                query="UPDATE \"Buses\""+\
                'SET "Modelo" ='"'"+str(Modelo)+"'"+\
                    ',"Capacidad"='"'"+str(Capacidad)+"'"+\
                    ',"Placa"='"'"+Placa+"'"+\
                    ',"Marca"='"'"+Marca+"'"+\
                'WHERE "Id" = '+str(Id)
                self.conn.write_db(query)
                print("Bus Actualizado con éxito")
            except (Exception, psycopg2.DatabaseError) as error:
                print(error)

    def read_bus(self):
        try:
            query ='SELECT * from \"Buses\" order by "Id";'
            buses=self.conn.search_db(query)
            return buses
        except (Exception, psycopg2.DatabaseError) as error:
                print(error)

    def delete_bus(self,Id):
        try:
            query="DELETE FROM \"Buses\""+\
                'WHERE "Id" = '+str(Id)
            self.conn.write_db(query)
            
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

#_________________________________________________________________
#PARA TABLA ESTACIONES

    def insert_station(self,Nombre,Direccion):
        try:
            query="INSERT INTO \"Estaciones\""+\
            '("Nombre", "Direccion")'+\
            'VALUES ('"'"+Nombre+"'"','"'"+Direccion+"'"')'
            self.conn.write_db(query)
            print("Estación ingresada con éxito")
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

    def update_station(self,Id,Nombre,Direccion):
            print ("Actualizando Bus")
            try:
                query="UPDATE \"Estaciones\""+\
                'SET "Nombre" ='"'"+Nombre+"'"+\
                    ',"Direccion"='"'"+Direccion+"'"+\
                    'WHERE "Id" = '+str(Id)
                self.conn.write_db(query)
                print("Estación actualizada con éxito")
            except (Exception, psycopg2.DatabaseError) as error:
                print(error)

    def read_station(self):
        try:
            query ='SELECT * from \"Estaciones\" order by "Id";'
            stations=self.conn.search_db(query)
            return stations
        except (Exception, psycopg2.DatabaseError) as error:
                print(error)

    def delete_station(self,Id):
        try:
            query="DELETE FROM \"Estaciones\""+\
                'WHERE "Id" = '+str(Id)
            self.conn.write_db(query)
            print("Estación eliminada con éxito")
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)